package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class AppController {
    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/greet")
    public String greet() {
        final String URI = "http://greeting-service/greet";
        String response = restTemplate.getForObject(URI, String.class);
        return response;
    }

    @GetMapping("/hello")
    public String hello(){
        final String URI = "http://hello-service/hello";
        String response = restTemplate.getForObject(URI, String.class);
        return response;
    }

    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping("/service-instances/{appName}")
    public List<ServiceInstance> serviceInstancesByApplicationName(@PathVariable String appName){
        return this.discoveryClient.getInstances(appName);
    }

    @LoadBalanced
    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }
}

